package gatewayBrand

type Env struct {
	Eth0Address  string  `json:"ip,omitempty"`
	Prefix       string  `json:"prefix,omitempty"`
	Eth0BAddress string  `json:"ipb,omitempty"`
	PrefixB      string  `json:"prefixB,omitempty"`
	PPPAddress   string  `json:"ppp,omitempty"`
	VPNAddress   string  `json:"vpn,omitempty"`
	GatewayIP    string  `json:"gw,omitempty"`
	GatewayPing  string  `json:"gwp,omitempty"`
	DNS          string  `json:"dns,omitempty"`
	Platform     string  `json:"platform,omitempty"`
	Build        string  `json:"build,omitempty"`
	Agent        string  `json:"agent,omitempty"`
	Watchdog     string  `json:"Watchdog,omitempty"`
	Lora         string  `json:"Lora,omitempty"`
	Openvpn      string  `json:"Openvpn,omitempty"`
	Monit        string  `json:"Monit,omitempty"`
	IotAgent     string  `json:"IotAgent,omitempty"`
	Sshd         string  `json:"Sshd,omitempty"`
	SshKey       int     `json:"sshKey,omitempty"`
	Serial       string  `json:"serial,omitempty"`
	Model        string  `json:"model,omitempty"`
	Latitude     float64 `json:"latitude,omitempty"`
	Longitude    float64 `json:"longitude,omitempty"`
	DF           string  `json:"diskfree,omitempty"`
	Uptime       string  `json:"uptime,omitempty"`
	//ProductID   string	 `json:"productID"`
	//EUI		 string	 `json:"eui"`
	NodeLora string `json:"nodeLora,omitempty"`
	IMEI     string `json:"IMEI,omitempty"`
	IMSI     string `json:",omitempty"`
	SIM      string `json:"SIM,omitempty"`
	Operator string `json:"Operator,omitempty"`
	Quality  string `json:"Quality,omitempty"`
}

type Platform interface {
	// Change location of important files to match the platform
	SetUp() error
	// MacAddr gets the MAC hardware address of the host machine
	MacAddr() string
	// Get the map with all files paths
	GetPaths() map[string]string
	// Lookup specific system information from gateway
	GetGatewayInformation(AddressVPN string) (Env, error)
	// Setup VPN (necessary for Kerlink gateway)
	SetupVPN() error
}