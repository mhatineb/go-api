package server

import (
	"GO-API/gatewayBrand"
	"fmt"
	"html/template"
	"net/http"
)

var (
	VERSION = "2.0."
	BUILD   = "1"
	COMMIT  = ""

	
	API_KEY_CC  = "em1LLgblwuqdtXlkPczK6qrpCreQBCEPEfDi8fg150tYx5DXSOg2VS0okCwYMbLs"
	API_REST_CC = "https://iot.requea.com/rest/customercode/"
)

type MainController struct {
	test *template.Template
	StatProvider StatProvider
	env          gatewayBrand.Env
	id           string
}

type testP struct {
	Mac           string
	VersionServer string
	Inf           []Info
	
}

type clientJson struct {
	Key string
	Url string
}

type Json struct {
	Server []clientJson
}

type StatProvider interface {
	Status() gatewayBrand.Env
}

func (mc *MainController) Init(url string) {
	
	

	VERSION = VERSION + BUILD + ":" + COMMIT
}

func (mc *MainController) TestF(w http.ResponseWriter, r *http.Request) {
	inf := mc.TestController()
	data := testP{mc.id,VERSION, inf}

	err := mc.test.Execute(w, data)
	if err != nil {
		fmt.Println(err)
	}
}