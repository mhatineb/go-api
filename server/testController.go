package server


type Info struct {
	Application string
	Lora        bool
	Watchdog    bool
	OpenVPN     bool
	Monit       bool
	IotAgent    bool
	Ssh         bool
	SshKey      int
}

func (mc *MainController) TestController() []Info {
	var lora, watchdog, openvpn, monit, iotAgent, ssh bool
	if mc.env.Lora == "ok" {
		lora = true
	}
	if mc.env.Watchdog == "ok" {
		watchdog = true
	}
	if mc.env.Openvpn == "ok" {
		openvpn = true
	}
	if mc.env.Monit == "ok" {
		monit = true
	}
	if mc.env.IotAgent == "ok" {
		iotAgent = true
	}
	if mc.env.Sshd == "ok" {
		ssh = true
	}
	inf := []Info{{
		Application: "iot.requea.com",
		Lora:        lora,
		Watchdog:    watchdog,
		OpenVPN:     openvpn,
		Monit:       monit,
		IotAgent:    iotAgent,
		Ssh:         ssh,
		SshKey:      mc.env.SshKey,
	}}
	return inf
}





	
	




