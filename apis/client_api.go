package apis

import (
	"errors"
	"net/http"
	"github.com/gin-gonic/gin"
)

// Client API with handler
type ClientAPI struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

var clientList = //make(map[string]Client)
				[]ClientAPI{
					{
						ID: "1",
						Name: "Marie",
					},
					{
						ID: "2",
						Name: "Josiane",
					},
				}


func (api *ClientAPI) GetClient(context *gin.Context) {
	context.IndentedJSON(http.StatusOK, clientList)
}

func (api *ClientAPI) AddClient(context *gin.Context) {
	var newClient ClientAPI
	
	if err := context.BindJSON(&newClient); err != nil {
		context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	
	clientList = append(clientList, newClient)
	context.IndentedJSON(http.StatusCreated, newClient)
	}
	
	func (api *ClientAPI) GetClientByIDHandler(context *gin.Context) {
		id := context.Param("id")
		client, err := getClientById(id)
		if err != nil {
			context.IndentedJSON(http.StatusNotFound, gin.H{"message": "Client not found"})
			return
		}
		context.IndentedJSON(http.StatusOK, client)
	}
	
	func getClientById(id string) (*ClientAPI, error) {
		for i, a := range clientList {
			if a.ID == id {
	
				return &clientList[i], nil
	
			}
		}
	
		return nil, errors.New("client not found")
	}
	
	func (api *ClientAPI) ToggleClientStatus(context *gin.Context) {
		id := context.Param("id") 
	
		client, err := getClientById(id) 
	
		if err != nil {
			context.IndentedJSON(http.StatusNotFound, gin.H{"message": "Client not found"})
			return
		}
	
		context.IndentedJSON(http.StatusOK, client)
	}
	
	func (api *ClientAPI) UpdateClient(context *gin.Context) {
		id := context.Param("id")
		var updatedClient ClientAPI
	
		if err := context.BindJSON(&updatedClient); err != nil {
			context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
	
		client, err := getClientById(id)
		if err != nil {
			context.IndentedJSON(http.StatusNotFound, gin.H{"message": "Client not found"})
			return
		}
	
		//Mettre à jour les champs de Client
		client.Name = updatedClient.Name
		
		
	
		context.IndentedJSON(http.StatusOK, client)
	}