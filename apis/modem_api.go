package apis

import (
	"errors"
	"net/http"
	"github.com/gin-gonic/gin"
)

//Modem API

type ModemAPI struct {
	ID string `json:"id"`
	Modem bool `json:"modem"`
}

var modemList = []ModemAPI{
	{
		ID: "1",
		Modem: false,
		
	},
	{
		ID: "2",
		Modem: false,
		
	},
	{
		ID: "3",
		Modem: true,
		
	},
	{
		ID: "4",
		Modem: true,
		// IMEI: 351626103377229,
		// Version: "M0F.670006",
		// SIM: false,
		// Comment: "",
	},

}
func (api *ModemAPI) GetModem(context *gin.Context) {
	context.IndentedJSON(http.StatusOK, modemList)
}

func (api *ModemAPI) AddModem(context *gin.Context) {
var newModem ModemAPI

if err := context.BindJSON(&newModem); err != nil {
	context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	return
}

modemList = append(modemList, newModem)
context.IndentedJSON(http.StatusCreated, newModem)
}

func (api *ModemAPI) GetModemByIDHandler(context *gin.Context) {
	id := context.Param("id")
	modem, err := getModemById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "modem not found"})
		return
	}
	context.IndentedJSON(http.StatusOK, modem)
}

func getModemById(id string) (*ModemAPI, error) {
	for i, a := range modemList {
		if a.ID == id {

			return &modemList[i], nil

		}
	}

	return nil, errors.New("modem not found")
}

func (api *ModemAPI) ToggleModemStatus(context *gin.Context) {
	id := context.Param("id") 

	modem, err := getModemById(id) 

	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "modem not found"})
		return
	}

	context.IndentedJSON(http.StatusOK, modem)
}

func (api *ModemAPI) UpdateModem(context *gin.Context) {
	id := context.Param("id")
	var updatedModem ModemAPI

	if err := context.BindJSON(&updatedModem); err != nil {
		context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	modem, err := getModemById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "modem not found"})
		return
	}

	//Mettre à jour les champs de Modem
	modem.ID = updatedModem.ID
	modem.Modem = updatedModem.Modem
	

	context.IndentedJSON(http.StatusOK, modem)
}
