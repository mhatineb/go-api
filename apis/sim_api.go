package apis

import (
	"errors"
	"net/http"
	"github.com/gin-gonic/gin"
)

//SIM API
type SIMAPI struct {
	ID string `json:"id"`;
	SIM bool `json:"sim"`;
}
	var SIMList = []SIMAPI {
	
	{ID: "1" ,
	SIM: false,
	},
	{ID: "2",
	SIM: true,
	},
	{ID: "3",
	SIM: true,
	},
	{ID: "4",
	SIM: false,
	},
}

func (api *SIMAPI) GetSIM(context *gin.Context) {
	context.IndentedJSON(http.StatusOK, SIMList)
}

func (api *SIMAPI) AddSIM(context *gin.Context) {
var newSIM SIMAPI

if err := context.BindJSON(&newSIM); err != nil {
	context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	return
}
}

func (api *SIMAPI) GetSIMByIDHandler(context *gin.Context) {
	id := context.Param("id")
	sim, err := getSIMById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "sim not found"})
		return
	}
	context.IndentedJSON(http.StatusOK, sim)
}

func getSIMById(id string) (*SIMAPI, error) {
	for i, a := range SIMList {
		if a.ID == id {

			return &SIMList[i], nil

		}
	}

	return nil, errors.New("sim not found")
}

func (api *SIMAPI) ToggleSIMStatus(context *gin.Context) {
	id := context.Param("id")

	sim, err := getSIMById(id)

	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "sim not found"})
		return
	}

	context.IndentedJSON(http.StatusOK, sim)
} 

func (api *SIMAPI) UpdateSIM(context *gin.Context) {
	id := context.Param("id")
	var updatedSIM SIMAPI

	if err := context.BindJSON(&updatedSIM); err != nil {
		context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	sim, err := getSIMById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "sim not found"})
		return
	}

	// Mettre à jour les champs de la SIM
	sim.ID = updatedSIM.ID
	sim.SIM = updatedSIM.SIM
	

	context.IndentedJSON(http.StatusOK, sim)
}