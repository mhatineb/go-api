package apis

import (
	"errors"
	"net/http"
	"github.com/gin-gonic/gin"
)

//Version API
type VersionAPI struct {
	ID string `json:"id"`
	Version string `json:"version"`
 }

 var versionList = []VersionAPI{
		{
			ID:"1",
			Version: "M0F.425453",
		},
		{
			ID:"2",
			Version: "M0F.787897",
		},
		{
			ID:"3",
			Version: "M0F.456545",
		},
		{
			ID:"4",
			Version: "M0F.123123",
		},
 }
func (api *VersionAPI) GetVersion(context *gin.Context) {
	context.IndentedJSON(http.StatusOK, versionList)
}

func (api *VersionAPI) AddVersion(context *gin.Context) {
var newVersion VersionAPI

if err := context.BindJSON(&newVersion); err != nil {
	context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	return
}
}

func (api *VersionAPI) GetVersionByIDHandler(context *gin.Context) {
	id := context.Param("id")
	version, err := getVersionById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "version not found"})
		return
	}
	context.IndentedJSON(http.StatusOK, version)
}


func getVersionById(id string) (*VersionAPI, error) {
	for i, a := range versionList {
		if a.ID == id {

			return &versionList[i], nil

		}
	}

	return nil, errors.New("version not found")
}

func (api *VersionAPI) ToggleVersionStatus(context *gin.Context) {
	id := context.Param("id")

	version, err := getVersionById(id)

	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "version not found"})
		return
	}

	context.IndentedJSON(http.StatusOK, version)
}

func (api *VersionAPI) UpdateVersion(context *gin.Context) {
	id := context.Param("id")
	var updatedVersion VersionAPI

	if err := context.BindJSON(&updatedVersion); err != nil {
		context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	version, err := getVersionById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "version not found"})
		return
	}

	// Mettre à jour les champs de la version
	version.ID = updatedVersion.ID
	version.Version = updatedVersion.Version
	

	context.IndentedJSON(http.StatusOK, version)
}