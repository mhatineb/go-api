package apis

import (
	"errors"
	"net/http"
	"github.com/gin-gonic/gin"
)

//IMEI API

type IMEIAPI struct {
	ID string `json:"id"`
	IMEI int `json:"imei"`
}

var IMEIList = []IMEIAPI{
	{
		ID: "1",
		IMEI: 3545468546,
		
	},
	{
		ID: "2",
		IMEI: 6444778,
		
	},
	{
		ID: "3",
		IMEI: 8989898,
		
	},
	{
		ID: "4",
		IMEI: 8778787987984,
		
	},

}
func (api *IMEIAPI) GetIMEI(context *gin.Context) {
	context.IndentedJSON(http.StatusOK, IMEIList)
}

func (api *IMEIAPI) AddIMEI(context *gin.Context) {
var newImei IMEIAPI

if err := context.BindJSON(&newImei); err != nil {
	context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	return
}

IMEIList = append(IMEIList, newImei)
context.IndentedJSON(http.StatusCreated, newImei)
}

func (api *IMEIAPI) GetIMEIByIDHandler(context *gin.Context) {
	id := context.Param("id")
	IMEI, err := getIMEIById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "imei not found"})
		return
	}
	context.IndentedJSON(http.StatusOK, IMEI)
}

func getIMEIById(id string) (*IMEIAPI, error) {
	for i, a := range IMEIList {
		if a.ID == id {

			return &IMEIList[i], nil

		}
	}

	return nil, errors.New("imei not found")
}

func (api *IMEIAPI) ToggleIMEIStatus(context *gin.Context) {
	id := context.Param("id") 

	IMEI, err := getIMEIById(id) 

	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "imei not found"})
		return
	}

	context.IndentedJSON(http.StatusOK, IMEI)
}

func (api *IMEIAPI) UpdateIMEI(context *gin.Context) {
	id := context.Param("id")
	var updatedIMEI IMEIAPI

	if err := context.BindJSON(&updatedIMEI); err != nil {
		context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	IMEI, err := getIMEIById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "imei not found"})
		return
	}

	//Mettre à jour les champs de IMEI
	IMEI.ID = updatedIMEI.ID
	IMEI.IMEI = updatedIMEI.IMEI
	

	context.IndentedJSON(http.StatusOK, IMEI)
}