
package apis

import (
	"errors"
	"net/http"
	"github.com/gin-gonic/gin"
)

//APN API

type ApnAPI struct {
	ID       string `json:"id"`
	Operator string `json:"operator"`
	Mnc      int    `json:"mnc"`
	Mcc      int    `json:"mcc"`
	Apn      string `json:"apn"`
	Username string `json:"username"`
	Password string `json:"password"`
}

var apnList = []ApnAPI{
	{
		ID:       "1",
		Operator: "Orange",
		Apn:      "orange.m2m.spec",
		Mnc:      1,
		Mcc:      208,
		Username: "*",
		Password: "*",
	},
	{
		ID:       "2",
		Operator: "NumeriSat",
		Apn:      "fnetnrj",
		Mnc:      26,
		Mcc:      208,
		Username: "*",
		Password: "*",
	},
	{
		ID:       "3",
		Operator: "SFR-Roaming",
		Apn:      "m2minternet",
		Mnc:      06,
		Mcc:      268,
		Username: "*",
		Password: "*",
	},
}

func (api *ApnAPI) GetApn(context *gin.Context) {
	context.IndentedJSON(http.StatusOK, apnList)
}

func (api *ApnAPI) AddApn(context *gin.Context) {
	var newApn ApnAPI

	if err := context.BindJSON(&newApn); err != nil {
		context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	apnList = append(apnList, newApn)
	context.IndentedJSON(http.StatusCreated, newApn)
}

func (api *ApnAPI) GetApnByIDHandler(context *gin.Context) {
	id := context.Param("id")
	ApnAPI, err := getApnById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "Apn not found"})
		return
	}
	context.IndentedJSON(http.StatusOK, ApnAPI)
}

func getApnById(id string) (*ApnAPI, error) {
	for i, a := range apnList {
		if a.ID == id {

			return &apnList[i], nil

		}
	}

	return nil, errors.New("APN not found")
}

func (api *ApnAPI) ToggleApnStatus(context *gin.Context) {
	id := context.Param("id") 

	apn, err := getApnById(id) 

	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "Apn not found"})
		return
	}

	context.IndentedJSON(http.StatusOK, apn)
}

func (api *ApnAPI) UpdateApn(context *gin.Context) {
	id := context.Param("id")
	var updatedApn ApnAPI

	if err := context.BindJSON(&updatedApn); err != nil {
		context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	apn, err := getApnById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "apn not found"})
		return
	}

	// Mettre à jour les champs de l'Apn
	apn.ID = updatedApn.ID
	apn.Operator = updatedApn.Operator
	apn.Mnc = updatedApn.Mnc
	apn.Mcc = updatedApn.Mcc
	apn.Username = updatedApn.Username
	apn.Password = updatedApn.Password


	context.IndentedJSON(http.StatusOK, apn)
}
