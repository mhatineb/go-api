package apis

import (
	"errors"
	"net/http"
	"github.com/gin-gonic/gin"
)


// IpAddresses API
type IpAddAPI struct {
	ID string `json:"id"`
	Name string `json:"name"`
	Address string `json:"address"`

}
var ipList =  []IpAddAPI {
		  { ID:"1", Name: "Eth0", Address: "10.139.0.161" },

          { ID:"2", Name: "Eth0.1", Address: "10.139.0.161" },

          { ID:"3", Name: "Tun0", Address: "10.139.0.161" },
}

func (api *IpAddAPI) GetIpAddress(context *gin.Context) {
	context.IndentedJSON(http.StatusOK, ipList)
}

func (api *IpAddAPI) AddIpAddress(context *gin.Context) {
	var newIpAdd IpAddAPI

	if err := context.BindJSON(&newIpAdd); err != nil {
		context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ipList = append(ipList, newIpAdd)
	context.IndentedJSON(http.StatusCreated, newIpAdd)
}

func (api *IpAddAPI) GetIpAddByIDHandler(context *gin.Context) {
	id := context.Param("id")
	ipAdd, err := getIpAddById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "IpAddress not found"})
		return
	}
	context.IndentedJSON(http.StatusOK, ipAdd)
}

func getIpAddById(id string) (*IpAddAPI, error) {
	for i, a := range ipList {
		if a.ID == id {

			return &ipList[i], nil

		}
	}

	return nil, errors.New("IpAdress not found")
}

func (api *IpAddAPI) ToggleIpAddStatus(context *gin.Context) {
	id := context.Param("id")

	ipAdd, err := getIpAddById(id)

	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "ipAddress not found"})
		return
	}

	context.IndentedJSON(http.StatusOK, ipAdd)
}

func (api *IpAddAPI) UpdateIpAdd(context *gin.Context) {
	id := context.Param("id")
	var updatedIpAdd IpAddAPI

	if err := context.BindJSON(&updatedIpAdd); err != nil {
		context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ipAdd, err := getIpAddById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "ipAddress not found"})
		return
	}

	// Mettre à jour les champs de l'ipAdd
	ipAdd.ID = updatedIpAdd.ID
	ipAdd.Name = updatedIpAdd.Name
	ipAdd.Address = updatedIpAdd.Address

	context.IndentedJSON(http.StatusOK, ipAdd)
}
