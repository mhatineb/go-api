package apis

import (
	"errors"
	"net/http"
	"github.com/gin-gonic/gin"
)

//Test API
type TestAPI struct {
	ID string `json:"id"`
	Status bool `json:"status"`
	IconColor string `json:"iconColor"`
	IconClass string `json:"iconClass"`
	InfoIconClass string `json:"infoIconClass"`
	NbrSSH int `json:"nbrSSH"`
}

var testList = []TestAPI{

	{ID: "1",
	 Status: true,
	 IconColor: "bg-red-500",
	 IconClass: "far fa-chart-bar",
	 InfoIconClass: "fas fa-arrow-up",
	 NbrSSH: 15465468764516354,
	},
	{
	ID: "2",
	Status: false,
	IconColor: "bg-green-500",
	IconClass: "fas fa-chart-pie",
	InfoIconClass: "fas fa-arrow-down",
	NbrSSH: 764511515,
	
	},
}

func (api *TestAPI) GetTest(context *gin.Context) {
	context.IndentedJSON(http.StatusOK, testList)
}

func (api *TestAPI) AddTest(context *gin.Context) {
var newTest TestAPI

if err := context.BindJSON(&newTest); err != nil {
	context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	return
}

testList = append(testList, newTest)
context.IndentedJSON(http.StatusCreated, newTest)
}

func (api *TestAPI) GetTestByIDHandler(context *gin.Context) {
id := context.Param("id")
TestTable, err := getTestById(id)
if err != nil {
	context.IndentedJSON(http.StatusNotFound, gin.H{"message": "Test not found"})
	return
}
context.IndentedJSON(http.StatusOK, TestTable)
}

func getTestById(id string) (*TestAPI, error) {
for i, a := range testList {
	if a.ID == id {

		return &testList[i], nil

	}
}

return nil, errors.New("test not found")
}

func (api *TestAPI) ToggleTestStatus(context *gin.Context) {
id := context.Param("id") 

test, err := getTestById(id) 

if err != nil {
	context.IndentedJSON(http.StatusNotFound, gin.H{"message": "test not found"})
	return
}

context.IndentedJSON(http.StatusOK, test)
}

func (api *TestAPI) UpdateTest(context *gin.Context) {
id := context.Param("id")
var updatedTest TestAPI

if err := context.BindJSON(&updatedTest); err != nil {
	context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	return
}

test, err := getTestById(id)
if err != nil {
	context.IndentedJSON(http.StatusNotFound, gin.H{"message": "test not found"})
	return
}

// Mettre à jour les champs de Test
test.ID = updatedTest.ID
test.Status = updatedTest.Status
test.IconColor = updatedTest.IconColor
test.IconClass = updatedTest.IconClass
test.InfoIconClass = updatedTest.InfoIconClass
test.NbrSSH = updatedTest.NbrSSH

context.IndentedJSON(http.StatusOK, test)
}
