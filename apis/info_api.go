package apis

import (
	"errors"
	"net/http"
	"github.com/gin-gonic/gin"
)

//Info API
type InfosAPI struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	IsConnected bool `json:"isConnected"`

}

var infoList = []InfosAPI{
	{
		ID:          "1",
		Title:       "Connecté",
		Description: "02165451321684651321",
		IsConnected: true,
	},
	{
		ID:          "2",
		Title:       "N° de série",
		Description: "02165451321684651321",
		IsConnected: true,
	},
	{
		ID:          "3",
		Title:       "Depuis le:",
		Description: "11 may 2023 à 20:15:45",
		IsConnected: true,
	},
	{
		ID:          "4",
		Title:       "Version actuelle:",
		Description: "V-11",
		IsConnected: true,
	},
}

func (api *InfosAPI) GetInfo(context *gin.Context) {
	context.IndentedJSON(http.StatusOK, infoList)
}

func (api *InfosAPI) AddInfo(context *gin.Context) {
	var newInfo InfosAPI

	if err := context.BindJSON(&newInfo); err != nil {
		context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	infoList = append(infoList, newInfo)
	context.IndentedJSON(http.StatusCreated, newInfo)
}

func (api *InfosAPI) GetInfoByIDHandler(context *gin.Context) {
	id := context.Param("id")
	Info, err := GetInfoById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "info not found"})
		return
	}
	context.IndentedJSON(http.StatusOK, Info)
}

func GetInfoById(id string) (*InfosAPI, error) {
	for i, a := range infoList {
		if a.ID == id {

			return &infoList[i], nil

		}
	}

	return nil, errors.New("info not found")
}

func (api *InfosAPI) ToggleInfoStatus(context *gin.Context) {
	id := context.Param("id")

	info, err := GetInfoById(id)

	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "info not found"})
		return
	}

	context.IndentedJSON(http.StatusOK, info)
}

func (api *InfosAPI) UpdateInfo(context *gin.Context) {
	id := context.Param("id")
	var updatedInfo InfosAPI

	if err := context.BindJSON(&updatedInfo); err != nil {
		context.IndentedJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	info, err := GetInfoById(id)
	if err != nil {
		context.IndentedJSON(http.StatusNotFound, gin.H{"message": "info not found"})
		return
	}

	// Mettre à jour les champs de l'Info
	info.ID = updatedInfo.ID
	info.Title = updatedInfo.Title
	info.Description = updatedInfo.Description

	context.IndentedJSON(http.StatusOK, info)
}