package routers

import (
	"GO-API/apis"
	"github.com/gin-gonic/gin"
)

func SetupInfoRouter(router *gin.Engine) {
	infoAPI := &apis.InfosAPI{}

	infoGroup := router.Group("/api/infos")
	{
		infoGroup.GET("", infoAPI.GetInfo)
		infoGroup.GET("/:id", infoAPI.GetInfoByIDHandler)
		infoGroup.PUT("/:id", infoAPI.UpdateInfo)
		infoGroup.PATCH("/:id", infoAPI.ToggleInfoStatus)
		infoGroup.POST("", infoAPI.AddInfo)
	}
}