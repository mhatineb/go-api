package routers

import (
	"GO-API/apis"
	"github.com/gin-gonic/gin"
)

func SetupApnRouter(router *gin.Engine) {
	apnAPI := &apis.ApnAPI{}

	apnGroup := router.Group("/api/apn")
	{
		apnGroup.GET("", apnAPI.GetApn)
		apnGroup.GET("/:id", apnAPI.GetApnByIDHandler)
		apnGroup.PUT("/:id", apnAPI.UpdateApn)
		apnGroup.PATCH("/:id", apnAPI.ToggleApnStatus)
		apnGroup.POST("", apnAPI.AddApn)
	}
}