package routers

import (
	"GO-API/apis"
	"github.com/gin-gonic/gin"
)

func SetupIpAddressRouter(router *gin.Engine) {
	IpAddressAPI := &apis.IpAddAPI{}

	IpAddressGroup := router.Group("/api/ipaddress")
	{
		IpAddressGroup.GET("", IpAddressAPI.GetIpAddress)
		IpAddressGroup.GET("/:id", IpAddressAPI.GetIpAddByIDHandler)
		IpAddressGroup.PUT("/:id", IpAddressAPI.UpdateIpAdd)
		IpAddressGroup.PATCH("/:id", IpAddressAPI.ToggleIpAddStatus)
		IpAddressGroup.POST("", IpAddressAPI.AddIpAddress)
	}
}