package routers


import (
	"GO-API/apis"
	"github.com/gin-gonic/gin"
)


func SetupConnectRouters(router *gin.Engine) {
	modemAPI := &apis.ModemAPI{}
	imeiAPI := &apis.IMEIAPI{}
	versionAPI := &apis.VersionAPI{}
	simAPI := &apis.SIMAPI{}

	connectGroup := router.Group("/api/connect")
	{
		// Modem routes
		connectGroup.GET("/modem", modemAPI.GetModem)
		connectGroup.GET("/modem/:id", modemAPI.GetModemByIDHandler)
		connectGroup.PUT("/modem/:id", modemAPI.UpdateModem)
		connectGroup.PATCH("/modem", modemAPI.ToggleModemStatus)
		connectGroup.POST("/modem", modemAPI.AddModem)

		// IMEI routes
		connectGroup.GET("/imei", imeiAPI.GetIMEI)
		connectGroup.GET("/imei/:id", imeiAPI.GetIMEIByIDHandler)
		connectGroup.PUT("/imei/:id", imeiAPI.UpdateIMEI)
		connectGroup.PATCH("/imei", imeiAPI.ToggleIMEIStatus)
		connectGroup.POST("/imei", imeiAPI.AddIMEI)

		// Version routes
		connectGroup.GET("/version", versionAPI.GetVersion)
		connectGroup.GET("/version/:id", versionAPI.GetVersionByIDHandler)
		connectGroup.PUT("/version/:id", versionAPI.UpdateVersion)
		connectGroup.PATCH("/version", versionAPI.ToggleVersionStatus)
		connectGroup.POST("/version", versionAPI.AddVersion)

		// SIM routes
		connectGroup.GET("/sim", simAPI.GetSIM)
		connectGroup.GET("/sim/:id", simAPI.GetSIMByIDHandler)
		connectGroup.PUT("/sim/:id", simAPI.UpdateSIM)
		connectGroup.PATCH("/sim", simAPI.ToggleSIMStatus)
		connectGroup.POST("/sim", simAPI.AddSIM)
	}
}