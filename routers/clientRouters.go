package routers

import (
	"GO-API/apis"
	"github.com/gin-gonic/gin"
)

func SetupClientRouter(router *gin.Engine) {
	clientAPI := &apis.ClientAPI{}

	ClientGroup := router.Group("/api/client")
	{
		ClientGroup.GET("", clientAPI.GetClient)
		ClientGroup.GET("/:id", clientAPI.GetClientByIDHandler)
		ClientGroup.PUT("/:id", clientAPI.UpdateClient)
		ClientGroup.PATCH("/:id", clientAPI.ToggleClientStatus)
		ClientGroup.POST("", clientAPI.AddClient)
	}
}