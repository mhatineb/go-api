package routers

import (
	"GO-API/apis"
	"github.com/gin-gonic/gin"
)

func SetupTestRouter(router *gin.Engine) {
	testAPI := &apis.TestAPI{}

	testGroup := router.Group("/api/test")

	{
		testGroup.GET("", testAPI.GetTest)
		testGroup.GET("/:id", testAPI.GetTestByIDHandler)
		testGroup.PUT("/:id", testAPI.UpdateTest)
		testGroup.PATCH("/:id", testAPI.ToggleTestStatus)
		testGroup.POST("", testAPI.AddTest)
	}
}