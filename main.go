package main

import (

	"GO-API/server"
	"GO-API/routers"
	"net/http"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)


func GetTestController(c *gin.Context){
	controller := &server.MainController{}
	tests := controller.TestController()
	c.JSON(http.StatusOK, tests)
}

func main() {

	
	
	router := gin.Default()
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowMethods = []string{"GET", "POST", "PUT","PATCH","DELETE"}
	router.Use(cors.Default())

	//Infos routers
	routers.SetupInfoRouter(router)
	//APN routers
	routers.SetupApnRouter(router)
	//IpAdd routers
	routers.SetupIpAddressRouter(router)
	//Test routers
	routers.SetupTestRouter(router)
	// Connect routers
	routers.SetupConnectRouters(router)
	//Client routers
	routers.SetupClientRouter(router)

	router.Run("localhost:8080")
	
}
